package moodee.moodee;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ViewFlipper;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class SignUpActivity extends Activity  {
    private Button nextButton, closeButton;
    private EditText phoneNumber, secretCode;
    private ViewFlipper signUpSteps;
    private Context context;
    private int nbSteps, actualStep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getWindow().setBackgroundDrawableResource(R.drawable.bgsignup);
        closeButton = (Button) findViewById(R.id.closeButton);
        nextButton = (Button) findViewById(R.id.nextButton);
        context = this;
        signUpSteps = (ViewFlipper) findViewById(R.id.signUpPhaseFlipper);
        nbSteps = (int)signUpSteps.getChildCount();
        actualStep = (int)signUpSteps.getDisplayedChild() + 1 ;
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        secretCode = (EditText) findViewById(R.id.password);

        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(actualStep == 1) {
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    previousStep();
                    if(actualStep > 1) {
                        closeButton.setText("<");
                    } else {
                        closeButton.setText("X");
                    }
                }
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualStep != nbSteps) {
                    nextStep();
                } else {
                    validateForm();
                }
                if(actualStep > 1) {
                    closeButton.setText("<");
                    secretCode.requestFocus();
                } else {
                    closeButton.setText("X");
                    phoneNumber.requestFocus();
                }
            }
        });
        phoneNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    nextStep();
                    closeButton.setText("<");
                    secretCode.requestFocus();
                    return true;
                }
                return false;
            }
        });

        secretCode.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    validateForm();
                    return true;
                }
                return false;
            }
        });
    }

    private void previousStep() {
        signUpSteps.setInAnimation(AnimationUtils.loadAnimation(context, R.anim.right_in));
        signUpSteps.setOutAnimation(AnimationUtils.loadAnimation(context, R.anim.right_out));
        signUpSteps.showPrevious();
        actualStep = (int) signUpSteps.getDisplayedChild() + 1;
    }

    private void nextStep() {
        signUpSteps.setInAnimation(AnimationUtils.loadAnimation(context, R.anim.left_in));
        signUpSteps.setOutAnimation(AnimationUtils.loadAnimation(context, R.anim.left_out));
        signUpSteps.showNext();
        actualStep = (int) signUpSteps.getDisplayedChild() + 1;
    }

    private void validateForm() {
        System.out.println("Validating !");

        final String username = phoneNumber.getText().toString();
        final String password = secretCode.getText().toString();

        /* Searching if user already exists */
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", username);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    // The query was successful.

                    if (objects.size() > 0) {
                    /* someone found => login */

                    ParseUser.logInInBackground(username, password, new LogInCallback() {
                        public void done(ParseUser user, ParseException e) {
                            if (user != null) {
                                // Hooray! The user is logged in.
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                // Signup failed. Look at the ParseException to see what happened.
                                System.out.println(username);
                                System.out.println(password);
                                e.printStackTrace();
                            }
                        }
                    });
                    } else {
                        /* no one found => sign up */
                        ParseUser user = new ParseUser();
                        user.setUsername(username);
                        user.setPassword(password);

                        user.signUpInBackground(new SignUpCallback() {
                            public void done(ParseException e) {
                                if (e == null) {
                                    // Hooray! Let them use the app now.
                                    setResult(RESULT_OK);
                                    finish();
                                } else {
                                    // Sign up didn't succeed. Look at the ParseException
                                    // to figure out what went wrong
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                } else {
                    // Look at the ParseException
                    // to figure out what went wrong
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}

