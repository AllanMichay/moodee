package moodee.moodee;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import com.parse.ParseException;

import static moodee.moodee.R.layout.activity_main;



public class MainActivity extends Activity {
    private final GestureDetector detector = new GestureDetector(new SwipeGestureDetector());
    private final int moodColorsUp[] = { R.drawable.yellow_gradient, R.drawable.orange_gradient,  R.drawable.green_gradient, R.drawable.blue_green_gradient, R.drawable.blue_gradient};
    private final int moodColorsDown[] = { R.drawable.violet_pink_grad, R.drawable.violet_blue_gradient, R.drawable.orange_rose_gradient, R.drawable.red_gradient, R.drawable.red_violet_gradient};
    private final int moodTexts[] = {R.string.textMain0, R.string.textMain1, R.string.textMain2, R.string.textMain3, R.string.textMain4, R.string.textMain5, R.string.textMain6, R.string.textMain7, R.string.textMain8, R.string.textMain9};
    static final int MIN_DISTANCE = 100;
    private View base, sendBar;
    private int moodNumber = 0;
    private int screenHeight, screenWidth;
    private boolean isScrolling;
    private TextView textMain;
    private float downX, downY, upX, upY;
    private View closeButton, sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Enable Local Datastore.

        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        base = (View) findViewById(R.id.screenColor);
        sendBar = (View) findViewById(R.id.sendBar);
        textMain = (TextView) findViewById(R.id.textMain);
        closeButton = (View) findViewById(R.id.closeButton);
        sendButton = (View) findViewById(R.id.sendButton);


        ParseUser currentUser = ParseUser.getCurrentUser();
        /* check if there is already a user */
        if(currentUser == null) {
            /* if there is no user : launch tuto */
            Intent intent = new Intent(this, TutorialActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            /* if there is a user : GO GO GO */
            //ParseUser.logOut();

            calculateScreenHeightWidth();
            base.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(final View view, final MotionEvent event) {
                    if (detector.onTouchEvent(event)) {
                        return true;
                    }
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        upX = event.getX();
                        upY = event.getY();
                        float deltaX = downX - upX;
                        float deltaY = downY - upY;
                        if(isScrolling && (Math.abs(deltaY) > Math.abs(deltaX))) {
                            isScrolling  = false;
                            displayMessage();
                        }
                        else {
                            if(downX < upX) {
                                launchReceiveMoodeeActivity();
                            }
                            else {
                                launchFriendListActivity();
                            }
                        }
                    }
                    return true;
                }
            });

            closeButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    base.setBackgroundResource(0);
                    hideTextMessage();
                }
            });
        }
    }

    private void launchReceiveMoodeeActivity() {
        Intent intent = new Intent(MainActivity.this, ReceiveMoodeeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    private void launchFriendListActivity() {
        Intent intent = new Intent(MainActivity.this, FriendListActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }

    private void displayMessage() {
        textMain.setAlpha(1);
        sendBar.setAlpha(1);
    }

    private void calculateScreenHeightWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;
    }

    class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {


        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            try {
                if(Math.abs(e2.getX() - e1.getX()) < MIN_DISTANCE && e1.getX() > screenWidth/6 && e1.getX() < 5*screenWidth/6) {
                    isScrolling = true;
                    hideTextMessage();
                    moodNumber = (int) ((e2.getY() - e1.getY()) / (screenHeight - e1.getY()) * 100);
                    if (moodNumber > 50) moodNumber = 50;
                    else if (moodNumber < -50) moodNumber = -50;
                    changeBackgroundColor();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }



        @Override
        public boolean onDown(MotionEvent e) {
            downX = e.getX();
            downY = e.getY();
            return true;
        }
    }

    private void hideTextMessage() {
        textMain.setAlpha(0);
        sendBar.setAlpha(0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeBackgroundColor() {
        Drawable bg = null;
        int cellNumber = (int) (moodNumber/10);
        if(cellNumber < 0) {
            cellNumber = - (cellNumber + 1);
            bg = getDrawable(moodColorsUp[cellNumber]);
            cellNumber = - (cellNumber + 1);
        }
        else {
            cellNumber -= 1;
            bg = getDrawable(moodColorsDown[cellNumber]);
        }
        cellNumber += 5;
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            base.setBackgroundDrawable(bg);
        } else {
            base.setBackground(bg);
        }
        changeText(cellNumber);
    }

    private void changeText(int cellNumber) {
        String text = (String) getString(moodTexts[cellNumber]);
        textMain.setText(text);
    }
}


/* sending Mood -- in dev */
//ParseObject mood = new ParseObject("Mood");
//mood.put("moodNumber", 1); /* number of mood */
//mood.put("isOpened", false);
//mood.put("sender", currentUser);
//mood.put("receiver", ParseObject.createWithoutData("_User", "IwkA1byZlG")); /* id of friend */
//mood.saveInBackground();

/* Adding a friend */
//ParseObject friendship = new ParseObject("Friendship");
//friendship.put("friend_first", currentUser);
//friendship.put("friend_second", ParseObject.createWithoutData("_User", "IwkA1byZlG")); /* id of friend */
//friendship.saveInBackground();
