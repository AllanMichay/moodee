package moodee.moodee.Model;

import android.net.Uri;

import moodee.moodee.R;

/**
 * Created by Allan on 17/06/2015.
 */
public class Contact {
    private String name;
    private Uri imagePath;
    private String number;

    public Contact(String name, String number) {
        this.name = name;
        this.number = number;
        this.imagePath =  Uri.parse("android.resource://moodee.moodee/" + R.drawable.base_profil);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Uri getImagePath() {
        return imagePath;
    }

    public void setImagePath(Uri imagePath) {
        this.imagePath = imagePath;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
