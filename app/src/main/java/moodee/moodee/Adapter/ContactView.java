package moodee.moodee.Adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import moodee.moodee.Model.Contact;
import moodee.moodee.R;

/**
 * Created by Allan on 17/06/2015.
 */
public class ContactView extends RelativeLayout {
    private TextView mContactName;
    private ImageView mIcon;

    public ContactView(Context c) {
        this(c, null);
    }

    public ContactView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public static ContactView inflate(ViewGroup parent) {
        ContactView contactView = (ContactView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_contact_fragment, parent, false);
        return contactView;
    }

    public ContactView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(R.layout.contact_fragment, this, true);
        setupChildren();
    }

    private void setupChildren() {
        mContactName = (TextView) findViewById(R.id.nameContact);
        mIcon = (ImageView) findViewById(R.id.imageContact);
    }

    public void setContact(Contact contact) {
        mContactName.setText(contact.getName());
        mIcon.setImageURI(contact.getImagePath());
    }
}
