package moodee.moodee.Adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import moodee.moodee.Model.Contact;
import moodee.moodee.R;

/**
 * Created by Allan on 17/06/2015.
 */
public class ContactAdapter extends BaseAdapter {
    private List<Contact> listContact;
    private LayoutInflater layoutInflater;

    public ContactAdapter(Context context, List<Contact> contacts) {
        this.listContact = new ArrayList<Contact>(contacts);
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ContactAdapter(Context context) {
        this.listContact = new ArrayList<Contact>();
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listContact.size();
    }

    @Override
    public Object getItem(int position) {
        return listContact.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public List<Contact> getListContact() {
        return listContact;
    }

    public void setListContact(List<Contact> listContact) {
        this.listContact = listContact;
    }

    public void addContact(Contact contact) {
        this.listContact.add(contact);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ContactView contactView = (ContactView)convertView;
        if (contactView == null) {
            contactView = contactView.inflate(parent);
        }
        Contact contact = this.listContact.get(position);
        contactView.setContact(contact);
        return contactView;
    }


}
