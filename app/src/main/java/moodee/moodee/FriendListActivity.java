package moodee.moodee;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import moodee.moodee.Adapter.ContactAdapter;
import moodee.moodee.Model.Contact;

import static moodee.moodee.R.id.listContacts;


public class FriendListActivity extends FragmentActivity {
    private final GestureDetector detector = new GestureDetector(new SwipeGestureDetector());
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private View backButton;
    private GridView gridViewContacts;
    private ContactAdapter adapter;
    private List<Contact> myList = new ArrayList<Contact>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);
        backButton = (View) findViewById(R.id.backButton);
        gridViewContacts = (GridView) findViewById(listContacts);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
                FriendListActivity.this.overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        });
        gridViewContacts.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
                detector.onTouchEvent(event);
                return true;
            }
        });
        getContactList();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    public void getContactList() {
        Cursor phonescursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        this.adapter = new ContactAdapter(this);
        while (phonescursor.moveToNext()) {
            String name = phonescursor
                    .getString(phonescursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            String photoPath = phonescursor
                    .getString(phonescursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

            String number = phonescursor
                    .getString(phonescursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

            String arr[] = name.split(" ", 2);
            name = arr[0];
            Contact contact = new Contact(name, number);
            if(photoPath != null) {
                Uri imagePath = Uri.parse(photoPath);
                contact.setImagePath(imagePath);
            }

            myList.add(contact);

        }
        phonescursor.close();
        this.adapter.setListContact(myList);
        this.gridViewContacts.setAdapter(adapter);
    }

    class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    backToMain();
                    return true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    }

    private void backToMain() {
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }
}
